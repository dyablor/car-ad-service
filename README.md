# Project info

Car-Ads is a web application written in Java, using the Spring Boot Framework to expose RESTful services which return different types of cars from an online used-car marketplace.

It uses in-memory H2 database to initialize and store pre-defined cars, and it also allows the user to add, modify or delete cars.

# Application development information

Development time: ~10 hours

Sources: stackoverflow.com, spring.io, github.com, thymeleaf.org, makeareadme.com

Stack: Spring Boot 2.3.4, Java 11

## Installation

To start the application, you have to run the CarAdsApplication from IntelliJ IDEA (or any other IDE of your choosing) and access the following link in your browser:

```
http://localhost:8080/
```

## RESTful API for Web App
This project exposes RESTful APIs which respond to the following HTTP requests:

#### POST: car-ads/ - adds a new car to the database
#### PUT: car-ads/{id} - changes the car entity with the specified id
#### DELETE: car-ads/{id} - deletes the car entity with the specified id
#### GET:

##### car-ads/all
Returns all the cars from the database

##### car-ads/{brand}/
Returns all the cars of the specified {brand}, if present in the database.

Accepts an optional parameter "year" that adds an additional filter. 
By adding it, all the cars of the specified {brand} manufactured in the specified {year} will be returned.

##### car-ads/filter
Accepts a parameter "year" and returns all the cars from the specified {year}.
The parameter is optional and is set to 2012 by default. 

## Examples for Web App

#### GET: car-ads/all
```
http://localhost:8080/car-ads/all
```

```JSON
[
    {"id":1,"brand":"VW","model":"Passat","condition":"used","year":2006,"price":7500.0},
    {"id":2,"brand":"VW","model":"Polo","condition":"used","year":2012,"price":6500.0},
    {"id":3,"brand":"BMW","model":"X5","condition":"used","year":2015,"price":30000.0}
    //...
]
```
##### GET: car-ads/{brand}/
```
http://localhost:8080/car-ads/bmw/
```

```JSON
[
    {"id":3,"brand":"BMW","model":"X5","condition":"used","year":2015,"price":30000.0},
    {"id":4,"brand":"BMW","model":"X1","condition":"used","year":2012,"price":15000.0}
]
```

#### GET: car-ads/{brand}/?year={year}

```
http://localhost:8080/car-ads/vw/?year=2012
```

```JSON
[
    {"id":2,"brand":"VW","model":"Polo","condition":"used","year":2012,"price":6500.0}
]
```

#### POST: car-ads/
###### Contet-Type: application/json 
###### Body:

```JSON
[
    { "id": 14, "brand": "VW", "model": "Golf", "condition": "used", "year": 2020, "price": 17500.0 }
]
```

#### PUT: car-ads/14
###### Contet-Type: application/json 
###### Body:

```JSON
[
    { "id": 15, "brand": "Lexus", "model": "LC", "condition": "new", "year": 2020, "price": 109000.0 }
]
```

## Future directions and improvements
Better test coverage 

Better error handling 

Added security

Login service and credentials storage in DB

Implement Telegram Bot