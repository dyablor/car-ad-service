package ro.robert.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarAdsApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(CarAdsApplication.class, args);
    }
    
}
