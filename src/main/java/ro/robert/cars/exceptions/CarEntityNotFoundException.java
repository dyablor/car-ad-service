package ro.robert.cars.exceptions;

public class CarEntityNotFoundException extends RuntimeException {
    public CarEntityNotFoundException(Long id) {
        super("Could not find entity with id: " + id);
    }
}
