package ro.robert.cars.exceptions;

import ro.robert.cars.data.entity.CarEntity;

public class CarEntityNotValidException extends RuntimeException {
    public CarEntityNotValidException(CarEntity carEntity) {
        super("Entity " + carEntity + " is not valid.");
    }
}
