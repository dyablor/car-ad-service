package ro.robert.cars.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.robert.cars.data.domain.CarEntityOperations;
import ro.robert.cars.data.entity.CarEntity;
import ro.robert.cars.exceptions.CarEntityNotFoundException;

import java.util.List;

@Service
public class CarEntityService {

    @Autowired
    private CarEntityOperations carEntityOperations;

    public CarEntity add(CarEntity carEntity) {
        return carEntityOperations.save(carEntity);
    }
    
    public CarEntity update(CarEntity newCarEntity, Long id) {
        return carEntityOperations.findById(id)
                .map(oldEntity -> {
                    oldEntity.setBrand(newCarEntity.getBrand());
                    oldEntity.setModel(newCarEntity.getModel());
                    oldEntity.setCondition(newCarEntity.getCondition());
                    oldEntity.setYear(newCarEntity.getYear());
                    oldEntity.setPrice(newCarEntity.getPrice());
                    return carEntityOperations.save(newCarEntity);
                })
                .orElseGet(() -> {
                    newCarEntity.setId(id);
                    return carEntityOperations.save(newCarEntity);
                });
    }

    public CarEntity getById(long id) {
        return carEntityOperations.findById(id).orElseThrow(() -> new CarEntityNotFoundException(id));
    }

    public void deleteById(long id) {
        carEntityOperations.deleteById(id);
    }

    public List<CarEntity> getAllCars() {
        return carEntityOperations.findAll();
    }

    public List<CarEntity> getAllCarsByYear(int year) {
        return carEntityOperations.findAllByYear(year);
    }

    public List<CarEntity> getAllByBrand(String brand) {
        return carEntityOperations.findAllByBrand(brand);
    }

    public List<CarEntity> filterByBrandAndYear(String brand, int year) {
        return carEntityOperations.findByBrandAndYear(brand, year);
    }
}
