package ro.robert.cars.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ro.robert.cars.data.entity.CarEntity;
import ro.robert.cars.exceptions.CarEntityNotValidException;
import ro.robert.cars.service.CarEntityService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/car-ads")
public class CarEntityController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CarEntityService carEntityService;

    @PostMapping("/")
    public CarEntity addCar(@RequestBody CarEntity newCarEntity) {
        if (newCarEntity.validateToPost()) {
            logger.error("Entity [{}] is not valid.", newCarEntity);
            throw new CarEntityNotValidException(newCarEntity);
        }

        logger.info("Entity [{}] was successfully added to the database.", newCarEntity);
        return carEntityService.add(newCarEntity);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CarEntity> updateCar(@RequestBody CarEntity newCarEntity, @PathVariable Long id) {
        if (newCarEntity.getId() == 0 || !newCarEntity.validateToPut()) {
            logger.error("Entity [{}] is not valid.", newCarEntity);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        CarEntity oldCarEntity = carEntityService.getById(id);
        carEntityService.update(newCarEntity, id);
        logger.info("Entity [{}] was successfully updated.", oldCarEntity);
        return new ResponseEntity<>(newCarEntity, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteEmployee(@PathVariable Long id) {
        carEntityService.deleteById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<CarEntity>> getAllCars() {
        List<CarEntity> carEntities = carEntityService.getAllCars();
        return validateResponseEntity(carEntities, "all");
    }

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public ResponseEntity<List<CarEntity>> getByYear(
            @RequestParam(name = "year", defaultValue = "2012") String year) {

        List<CarEntity> carsByYear = carEntityService.getAllCarsByYear(Integer.parseInt(year));

        return validateResponseEntity(carsByYear, year);
    }

    @RequestMapping(value = "/{brand}/", method = RequestMethod.GET)
    public ResponseEntity<List<CarEntity>> getByBrandAndYear(
            @PathVariable("brand") String brand,
            @RequestParam(name = "year", required = false) Integer year) {
        List<CarEntity> carEntities = year != null ? carEntityService.filterByBrandAndYear(brand.toUpperCase(), year) : carEntityService.getAllByBrand(brand.toUpperCase());
        return validateResponseEntity(carEntities, brand);
    }

    private ResponseEntity<List<CarEntity>> validateResponseEntity(List<CarEntity> carEntities, String param) {
        if (carEntities.isEmpty()) {
            logger.error("No cars are available for the parameter [{}].", param);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resources");
        }
        logger.info("All cars for the parameter [{}] were loaded from database: [{}]", param, Arrays.toString(carEntities.toArray()));
        return new ResponseEntity<>(carEntities, HttpStatus.OK);
    }
}
