package ro.robert.cars.data.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.robert.cars.data.entity.CarEntity;

import java.util.List;

public interface CarEntityOperations extends CrudRepository<CarEntity, Long> {

    @Query("SELECT cars FROM CarEntity cars WHERE cars.brand LIKE %?1%")
    List<CarEntity> findAllByBrand(String brand);

    @Query("SELECT cars FROM CarEntity cars WHERE cars.year = ?1")
    List<CarEntity> findAllByYear(int year);

    @Query("SELECT cars FROM CarEntity cars WHERE cars.brand LIKE %?1% AND cars.year = ?2")
    List<CarEntity> findByBrandAndYear(String brand, int year);

    List<CarEntity> findAll();
}
