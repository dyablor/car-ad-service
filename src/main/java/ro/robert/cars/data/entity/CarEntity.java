package ro.robert.cars.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

@Entity
@Table(name = "Cars")
public class CarEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private String model;

    @Column(nullable = false)
    private String condition;
    
    @Column(nullable = false)
    private int year;

    @Column(nullable = false)
    private double price;


    public CarEntity() {
    }

    public CarEntity(String brand, String model, int year, double price, ConditionType conditionType) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.price = price;
        this.condition = conditionType.getCondition();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    @Column(name = "condition")
    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public boolean validateToPost() {
        return !(getId() != 0 || year <= 0 || price <= 0 ||
                brand == null || model == null || condition == null);
    }

    public boolean validateToPut() {
        return !(getId() < 1 || year <= 0 || price <= 0 ||
                brand == null || model == null || condition == null);
    }

    @Override
    public String toString() {
        return "CarEntity{" +
                "id=" + getId() +
                ", year=" + year +
                ", price=" + price +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", condition='" + condition + '\'' +
                '}';
    }
}
