package ro.robert.cars.data.entity;

public enum ConditionType {
    NEW("new"), USED("used");

    public String condition;

    ConditionType(String condition) {
        this.condition = condition;
    }

    public String getCondition() {
        return condition;
    }
}
