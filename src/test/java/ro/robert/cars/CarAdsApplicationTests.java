package ro.robert.cars;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CarAdsApplication.class)
public class CarAdsApplicationTests {
    @Test
    void contextLoads() { }
}
