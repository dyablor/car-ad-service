package ro.robert.cars;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ro.robert.cars.service.CarEntityService;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = CarAdsApplication.class)
public class DbLoadTest {

    @Autowired
    private CarEntityService carEntityService;

    @Test
    public void testDataLoading() {
        assertEquals(12, carEntityService.getAllCars().size());
    }
}
