package ro.robert.cars;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import ro.robert.cars.data.domain.CarEntityOperations;
import ro.robert.cars.data.entity.CarEntity;
import ro.robert.cars.data.entity.ConditionType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;

@ContextConfiguration(classes = CarAdsApplication.class)
@DataJpaTest
public class CarEntityControllerTest {


    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CarEntityOperations carEntityOperations;


    @BeforeEach
    public void setUp() {
        CarEntity carEntity = new CarEntity("VW", "Jetta", 2020, 28000, ConditionType.NEW);
        testEntityManager.persist(carEntity);
    }

    @Test
    public void testFindByBrand() {
        List<CarEntity> carEntities = carEntityOperations.findByBrandAndYear("VW", 2020);
        assertFalse(carEntities.isEmpty());
    }

    @Test
    public void testFindByYear() {
        List<CarEntity> carEntities = carEntityOperations.findAllByYear(2020);
        assertFalse(carEntities.isEmpty());
    }

    @Test
    public void testCarAdd() {
        CarEntity newCarEntity = new CarEntity("BMW", "M5", 2020, 95000, ConditionType.NEW);
        carEntityOperations.save(newCarEntity);
        List<CarEntity> carEntities = carEntityOperations.findAllByBrand("BMW");
        System.out.println(carEntities.get(0).getBrand() + " " + carEntities.get(0).getPrice() + " " + carEntities.size());
        assertFalse(carEntities.isEmpty());
    }
}
